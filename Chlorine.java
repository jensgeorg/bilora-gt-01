package com.geotate.locationengine;

public class Chlorine {
    static {
        System.loadLibrary("Chlorine");
    }

    private native int jniStartup(String a, String b, String c, String d);
    private native int jniWaitForNextProgressEvent(int a, int[] b, byte[] c, int[] d);
    private native int jniGetVersionInfo(int[] a);
}
