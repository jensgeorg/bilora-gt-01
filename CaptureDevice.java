package com.geotate.captureunit;

public class CaptureDevice {
    public native int jniOpenDevice(int i, String s);
    public native int jniGetBatteryStatus(int[] i);
    public native int jniGetDeviceInfo(int[] i, java.lang.String[] s);
    public native int jniGetTrackCount(int[] i);
    public native int jniGetAvailableMemory(int[] i);
    public native int jniGetRTC(short[] s);
    public native int jniGetConfig(int[] i);
    public native int jniSetConfig(int[] i);
}
