#include <com_geotate_captureunit_CaptureDevice.h>

#include <jni.h>

#include <iostream>

JNIEXPORT jint JNICALL Java_com_geotate_captureunit_CaptureDevice_jniOpenDevice
    (JNIEnv *env, jobject object, jint a, jstring b)
{
    auto *clazz = env->GetObjectClass(object);
    auto *fid = env->GetFieldID(clazz, "devHandle", "I");
    env->SetIntField(object, fid, 915); // FIXME: Something non-zero for now

    if (b != nullptr) {
        std::cerr << "jniOpenDevice parameter b: ";
        const char *cstr = env->GetStringUTFChars(b, nullptr);
        std::cerr << cstr;
        env->ReleaseStringUTFChars(b, cstr);
    }

    return 0;
}

JNIEXPORT jint JNICALL Java_com_geotate_captureunit_CaptureDevice_jniGetBatteryStatus(
    JNIEnv *env, jobject object, jintArray array)
{
    return 0;
}

JNIEXPORT jint JNICALL Java_com_geotate_captureunit_CaptureDevice_jniGetDeviceInfo(JNIEnv *,
                                                                                   jobject,
                                                                                   jintArray,
                                                                                   jobjectArray)
{
    return 0;
}

JNIEXPORT jint JNICALL Java_com_geotate_captureunit_CaptureDevice_jniGetTrackCount(JNIEnv *,
                                                                                   jobject,
                                                                                   jintArray)
{
    return 0;
}

JNIEXPORT jint JNICALL Java_com_geotate_captureunit_CaptureDevice_jniGetAvailableMemory(JNIEnv *,
                                                                                        jobject,
                                                                                        jintArray)
{
    return 0;
}

JNIEXPORT jint JNICALL Java_com_geotate_captureunit_CaptureDevice_jniGetRTC(JNIEnv *,
                                                                            jobject,
                                                                            jshortArray)
{
    return 0;
}

JNIEXPORT jint JNICALL Java_com_geotate_captureunit_CaptureDevice_jniGetConfig(JNIEnv *,
                                                                               jobject,
                                                                               jintArray)
{
    // Index 5 of array is the capture delay in milliseconds
    return 0;
}

JNIEXPORT jint JNICALL Java_com_geotate_captureunit_CaptureDevice_jniSetConfig(JNIEnv *env,
                                                                               jobject,
                                                                               jintArray arr)
{
    // Index 5 of array is the capture delay in milliseconds
    jsize len = env->GetArrayLength(arr);
    jint *params = env->GetIntArrayElements(arr, nullptr);
    std::cerr << __PRETTY_FUNCTION__ << "Parameters passed:" << std::endl;
    for (int i = 0; i < len; i++) {
        std::cerr << " i: " << i << ", param: " << params[i] << std::endl;
    }
    return 0;
}
